<?php

use App\Http\Controllers\GameController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [GameController::class, 'index'])->name('game.play');
Route::get('/stats', [GameController::class, 'index'])->name('stats');

Route::prefix('game')->group(function() {
    Route::post('/start', [GameController::class, 'start'])->name('game.start');
    Route::post('/answer', [GameController::class, 'answer'])->name('game.answer');
    Route::get('/question', [GameController::class, 'question'])->name('game.question');
    Route::get('/over', [GameController::class, 'over'])->name('game.over');
});
