<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ isset($heading) ? $heading . ' - ' : '' }}Number guesser</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <style>
        body {
            font-family: 'Nunito';
            padding: 5rem;
        }

        .radio input[type="radio"] {
            opacity: 0;
            position: fixed;
            width: 0;
        }

        .radio label,
        button,
        .button {
            display: inline-block;
            background-color: #ddd;
            padding: 10px 20px;
            font-size: 16px;
            border: 2px solid #444;
            border-radius: 4px;
            text-align: center;
        }

        .radio label:hover,
        button:hover,
        .button:hover {
            background-color: #3f0c64;
            color: white;
        }

        p {
            margin: 0;
        }

        h1 {
            margin-bottom: 50px;
        }

        #possible-answers {
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }

        #possible-answers:hover {
            white-space: initial;
        }

        #footer {
            position: absolute;
            bottom: 10px;
            text-align: center;
            left: 0;
            right: 0;
            color: #929292;
        }

        a:visited,
        a:hover,
        a {
            color: #929292;
        }

        a {
            text-decoration: none;
        }

        a:hover:not(.button) {
            text-decoration: underline;
        }
    </style>
</head>
<body class="antialiased">
