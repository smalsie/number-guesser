@include('header')
<div style="float: left; width: 70%">
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <h1>{{ $question->getQuestion() }}</h1>

    <form action="{{ route('game.answer') }}" method="post" id="question-form">
        @csrf
        <div class="radio">
            @foreach($question->getPossibleAnswers() as $answer => $name)
                <label for="{{ $answer }}Answer">{{ $name }}</label>
                <input
                    type="radio"
                    name="answer"
                    id="{{ $answer }}Answer"
                    value="{{ $answer }}"
                    onclick="submitForm('question-form')">
            @endforeach
        </div>
        <input type="hidden" name="question" value="{{ $question->getIdentifier() }}">
        @foreach($question->getContext() as $key => $value)
            <input type="hidden" name="context[{{ $key }}]" value="{{ $value }}">
        @endforeach
    </form>

    <button onclick="location.reload()" style="margin-top: 50px;">Give me another question</button>
</div>
<div style="float: right; width: 30%; text-align: center;">
    <div>
        <h1>Questions asked:</h1>
        <p style="font-size: 75px;">{{ $game->guesses }}</p>
    </div>
    <div>
        <h2>Possible answers:</h2>
        <div id="possible-answers">
            @foreach($guesser->getPossibleValuesAsRanges() as $range)
                {{ $loop->first ? '' : ', ' }}
                {{ $range[0] }}
                @if(count($range) === 2)
                    - {{ $range[1] }}
                @endif
            @endforeach
        </div>
    </div>
</div>
@include('footer')
