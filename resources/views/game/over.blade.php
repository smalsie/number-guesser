@include('header')
<h1>Your number is:</h1>

<p style="font-size: 150px; margin-bottom: 25px;">{{ number_format($number) }}</p>

<a href="{{ route('game.play') }}" class="button">Play again</a>

<audio autoplay>
    <source src="/yay.mp3" type="audio/mpeg">
</audio>
@include('footer')
