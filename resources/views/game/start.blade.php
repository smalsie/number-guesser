@include('header')
<h1>Select a difficulty level</h1>

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{ route('game.start') }}" method="post" id="start-form">
    @csrf
    <div class="radio">
        @foreach($levels as $index => $level)
            <label for="{{ $index }}Answer">
                {{ $level['label'] }}<br>{{ number_format($level['range'][0]) }} - {{ number_format($level['range'][1]) }}
            </label>
            <input
                type="radio"
                name="level"
                id="{{ $index }}Answer"
                value="{{ $index }}"
                onclick="submitForm('start-form')">
        @endforeach
    </div>
</form>
@include('footer')
