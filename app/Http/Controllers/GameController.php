<?php

namespace App\Http\Controllers;

use App\Guesser;
use App\Models\Game;
use App\Questions\Question;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class GameController extends Controller
{
    private array $levels = [
        [
            'label' => 'Easy',
            'range' => [0, 100],
        ],
        [
            'label' => 'Medium',
            'range' => [0, 250],
        ],
        [
            'label' => 'Hard',
            'range' => [0, 1000],
        ],
        [
            'label' => 'Extreme',
            'range' => [0, 500000],
        ],
    ];

    public function index()
    {
        return view('game.start', [
            'levels' => $this->levels,
        ]);
    }

    public function start(Request $request)
    {
        $validatedData = $request->validate([
            'level' => [
                'required',
                Rule::in(array_keys($this->levels)),
            ],
        ]);

        $game = Game::create([
            'level' => $validatedData['level'],
        ]);

        $request->session()->put('gameId', $game->id);

        return redirect(route('game.question'));
    }

    public function question(Request $request)
    {
        $game = $this->getGame($request);
        $level = $this->levels[$game->level];

        $guesser = new Guesser($level['range'][0], $level['range'][1], $game->questions);

        if ($guesser->hasOneOptionLeft()) {
            return redirect(route('game.over'));
        }

        $question = $guesser->getQuestionToAsk($request->session()->get('questionClass'));

        $request->session()->put('questionClass', get_class($question));

        return view('game.question', compact('game', 'guesser', 'question'));
    }

    private function getGame(Request $request): Game
    {
        return Game::findOrFail($request->session()->get('gameId'));
    }

    public function answer(Request $request)
    {
        $validatedData = $request->validate([
            'answer' => 'required',
            'question' => 'required',
            'context' => 'array',
        ]);

        $game = $this->getGame($request);

        $question = Guesser::getValidQuestions()[$validatedData['question']];

        /** @var Question $question */
        $question = new $question();
        $question->setAnswer($validatedData['answer']);
        $question->setContext($validatedData['context'] ?? []);

        $game->addQuestion($question);
        $game->save();

        return redirect(route('game.question'));
    }

    public function over(Request $request)
    {
        $game = $this->getGame($request);
        $level = $this->levels[$game->level];

        $guesser = new Guesser($level['range'][0], $level['range'][1], $game->questions);

        $number = $guesser->getPossibleValues()[0];

        return view('game.over', compact('game', 'number'));
    }
}
