<?php

namespace App;

use App\Questions\EvenQuestion;
use App\Questions\GreaterThanQuestion;
use App\Questions\MultipleOfQuestion;
use App\Questions\NumberOfDigitsQuestion;
use App\Questions\Question;
use App\Questions\RandomGuessQuestion;

class Guesser
{
    private const MIN_MULTIPLE = 3;
    private const MAX_MULTIPLE = 10;
    private const MAX_VALUES_TO_RANGE = 500;

    private int $smallestValue;
    private int $biggestValue;
    private array $possibleValues;
    private array $answeredQuestions = [];
    private array $questions;

    public function __construct(int $smallestValue, int $biggestValue, array $questionAnswers = [])
    {
        $this->smallestValue = $smallestValue;
        $this->biggestValue = $biggestValue;

        $this->initialiseQuestions($questionAnswers);
        $this->setPossibleValues();
    }

    private function initialiseQuestions(array $questionAnswers): void
    {
        $this->questions = [
            EvenQuestion::getIdentifier() => EvenQuestion::class,
            NumberOfDigitsQuestion::getIdentifier() => NumberOfDigitsQuestion::class,
        ];

        /** @var Question $question */
        foreach ($questionAnswers as $question) {
            $this->answeredQuestions[] = $question;

            unset($this->questions[$question->getIdentifier()]);
        }
    }

    public static function getValidQuestions(): array
    {
        return [
            EvenQuestion::getIdentifier() => EvenQuestion::class,
            GreaterThanQuestion::getIdentifier() => GreaterThanQuestion::class,
            RandomGuessQuestion::getIdentifier() => RandomGuessQuestion::class,
            MultipleOfQuestion::getIdentifier() => MultipleOfQuestion::class,
            NumberOfDigitsQuestion::getIdentifier() => NumberOfDigitsQuestion::class,
        ];
    }

    public function getPossibleValues(): array
    {
        return $this->possibleValues;
    }

    public function setPossibleValues(): void
    {
        $this->possibleValues = array_keys(array_fill(
            $this->smallestValue,
            $this->biggestValue - ($this->smallestValue - 1),
            0
        ));

        /** @var Question $answeredQuestion */
        foreach ($this->answeredQuestions as $answeredQuestion) {
            $this->possibleValues = $answeredQuestion->filter($this->possibleValues);
        }
    }

    public function getPossibleValuesAsRanges(): array
    {
        $ranges = [];

        $startRange = null;
        $previousValue = null;

        foreach (array_slice($this->possibleValues, 0, self::MAX_VALUES_TO_RANGE) as $possibleValue) {
            if ($startRange === null) {
                $previousValue = $startRange = $possibleValue;

                continue;
            }

            if (($possibleValue - $previousValue) > 1) {
                $ranges[] = $this->addRange($startRange, $previousValue);

                $previousValue = $startRange = $possibleValue;

                continue;
            }

            $previousValue = $possibleValue;
        }

        $ranges[] = $this->addRange($startRange, $previousValue);

        if (count($this->possibleValues) > self::MAX_VALUES_TO_RANGE) {
            $ranges[] = [
                '...' . number_format(end($this->possibleValues))
            ];
        }

        return $ranges;
    }

    private function addRange(int $start, int $end): array
    {
        if ($start === $end) {
            return [number_format($start)];
        }

        return [
            number_format($start),
            number_format($end),
        ];
    }

    public function getQuestionToAsk(?string $lastQuestionClass): Question
    {
        $questions = [];

        if ($this->questions) {
            $questions = array_map(
                fn(string $class) => new $class(),
                $this->questions
            );
        }

        $questions = [...array_values($questions), ...$this->getQuestionsThatCanBeAskedMultipleTimes()];

        /** @var Question $question */
        foreach ($questions as $question) {
            $question->initialise($this->possibleValues);
        }

        $questions = array_filter(
            $questions,
            fn(Question $question) => $question->canBeAsked()
        );

        if ($lastQuestionClass) {
            $notAskedQuestions = array_filter(
                $questions,
                fn(Question $question) => !($question instanceof $lastQuestionClass)
            );
        }

        if ($notAskedQuestions ?? null) {
            $questions = $notAskedQuestions;
        }

        return $questions[array_rand($questions)];
    }

    private function getQuestionsThatCanBeAskedMultipleTimes(): array
    {
        $questions = [];

        if (count($this->possibleValues) < 5) {
            $questions[] = new RandomGuessQuestion();
        }

        if (count($this->possibleValues) > 2) {
            $questions[] = new GreaterThanQuestion();
        }

        if ($availableMultiples = $this->getAvailableMultiples()) {
            $question = new MultipleOfQuestion();
            $question->setAvailableMultiples($availableMultiples);

            $questions[] = $question;
        }

        return $questions;
    }

    public function getAvailableMultiples(): array
    {
        $multiples = range(self::MIN_MULTIPLE, self::MAX_MULTIPLE);

        $answeredMultiples = array_filter(
            $this->answeredQuestions,
            fn(Question $question) => $question instanceof MultipleOfQuestion
        );

        if (!$answeredMultiples) {
            return $multiples;
        }

        $answeredMultiples = array_map(
            fn(MultipleOfQuestion $question) => $question->getMultiple(),
            $answeredMultiples
        );

        return array_diff($multiples, $answeredMultiples);
    }

    public function hasOneOptionLeft(): bool
    {
        return count($this->possibleValues) === 1;
    }
}
