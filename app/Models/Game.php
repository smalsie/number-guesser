<?php

namespace App\Models;

use App\Guesser;
use App\Questions\Question;
use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    protected $casts = ['questions' => 'json'];
    protected $fillable = ['questions', 'level', 'questions_asked'];

    public static function boot()
    {
        parent::boot();

        static::updating(function (self $game) {
            $game->questions_asked = count($game->questions);
        });
    }
    public function getQuestionsAttribute(?string $questions): array
    {
        $questions = $questions ? json_decode($questions, true) : [];

        return array_map(
            function (array $questionData) {
                $questionClass  = Guesser::getValidQuestions()[$questionData['identifier']];
                unset($questionData['identifier']);

                /** @var Question $questionObject */
                $questionObject = new $questionClass();
                $questionObject->hydrate($questionData);

                return $questionObject;
            },
            $questions
        );
    }

    public function addQuestion(Question $question): void
    {
        $questions = $this->questions ?? [];
        $this->questions = [...$questions, $question];
    }

    public function getGuessesAttribute(): int
    {
        return count($this->questions) + 1;
    }
}
