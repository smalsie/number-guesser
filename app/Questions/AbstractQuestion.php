<?php

namespace App\Questions;

abstract class AbstractQuestion implements Question
{
    protected $answer;

    public function getPossibleAnswers(): array
    {
        return [
            true => 'Yes',
            false => 'No',
        ];
    }

    public function setAnswer($answer): void
    {
        $this->answer = (bool) $answer;
    }

    public function hydrate(array $data)
    {
        foreach ($data as $key => $value) {
            $this->{$key} = $value;
        }
    }

    public function jsonSerialize()
    {
        return [
            'identifier' => $this->getIdentifier(),
            'answer' => $this->answer,
        ];
    }

    public static function getIdentifier(): string
    {
        return sha1(static::class);
    }

    public function canBeAsked(): bool
    {
        return true;
    }

    public function initialise(array $possibleNumbers): void
    {
    }

    public function setContext(array $context): void
    {
    }

    public function getContext(): array
    {
        return [];
    }
}
