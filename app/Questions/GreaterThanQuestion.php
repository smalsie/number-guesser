<?php

namespace App\Questions;

class GreaterThanQuestion extends AbstractQuestion
{
    protected int $number;

    public function getQuestion(): string
    {
        return sprintf('Is your number greater than %s?', number_format($this->number));
    }

    public function initialise(array $possibleNumbers): void
    {
        array_shift($possibleNumbers);
        array_pop($possibleNumbers);

        $this->number = $possibleNumbers[array_rand($possibleNumbers)];
    }

    public function filter(array $possibleNumbers): array
    {
        $numbers = [];

        foreach ($possibleNumbers as $number) {
            $greaterThan = $number > $this->number;

            if ($greaterThan === $this->answer) {
                $numbers[] = $number;
            }
        }

        return $numbers;
    }

    public function jsonSerialize()
    {
        return parent::jsonSerialize() + [
            'number' => $this->number,
        ];
    }

    public function getContext(): array
    {
        return ['number' => $this->number];
    }

    public function setContext(array $context): void
    {
        $this->number = (int) $context['number'];
    }
}
