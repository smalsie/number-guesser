<?php

namespace App\Questions;

class MultipleOfQuestion extends AbstractQuestion
{
    protected int $multiple;
    private array $multiples;
    private array $possibleNumbers;

    public function getQuestion(): string
    {
        return sprintf('Is your number a multiple of %d?', $this->multiple);
    }

    public function initialise(array $possibleNumbers): void
    {
        $this->possibleNumbers = $possibleNumbers;

        $multiples = $this->multiples;
        shuffle($multiples);

        foreach ($possibleNumbers as $number) {
            foreach ($multiples as $multiple) {
                if (($number % $multiple) === 0) {
                    $this->multiple = $multiple;

                    return;
                }
            }
        }
    }

    public function filter(array $possibleNumbers): array
    {
        $numbers = [];

        foreach ($possibleNumbers as $number) {
            if ($number === 0) {
                $isMultiple = false;
            } else {
                $isMultiple = ($number % $this->multiple) === 0;
            }

            if ($isMultiple === $this->answer) {
                $numbers[] = $number;
            }
        }

        return $numbers;
    }

    public function jsonSerialize()
    {
        return parent::jsonSerialize() + [
                'multiple' => $this->multiple,
            ];
    }

    public function getContext(): array
    {
        return ['multiple' => $this->multiple];
    }

    public function setContext(array $context): void
    {
        $this->multiple = (int) $context['multiple'];
    }

    public function getMultiple(): int
    {
        return $this->multiple;
    }

    public function setAvailableMultiples(array $availableMultiples): void
    {
        $this->multiples = $availableMultiples;
    }

    public function canBeAsked(): bool
    {
        if (!isset($this->multiple)) {
            return false;
        }

        foreach ($this->possibleNumbers as $possibleValue) {
            if (($possibleValue % $this->multiple) !== 0) {
                return true;
            }
        }

        return false;
    }
}
