<?php

namespace App\Questions;

use JsonSerializable;

interface Question extends JsonSerializable
{
    public static function getIdentifier(): string;

    public function getQuestion(): string;

    public function getPossibleAnswers(): array;

    public function initialise(array $possibleNumbers): void;

    public function filter(array $possibleNumbers): array;

    public function setAnswer($answer): void;

    public function setContext(array $context): void;

    public function getContext(): array;

    public function hydrate(array $data);

    public function canBeAsked(): bool;
}
