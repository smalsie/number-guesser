<?php

namespace App\Questions;

class NumberOfDigitsQuestion extends AbstractQuestion
{
    private array $possibleDigits;

    public function getQuestion(): string
    {
        return 'How many digits does your number have?';
    }

    public function initialise(array $possibleNumbers): void
    {
        foreach ($possibleNumbers as $possibleNumber) {
            $this->possibleDigits[strlen($possibleNumber)] = 0;
        }

        $this->possibleDigits = array_keys($this->possibleDigits);
    }

    public function getPossibleAnswers(): array
    {
        $answers = [];

        foreach ($this->possibleDigits as $digit) {
            $answers[$digit] = $digit;
        }

        return $answers;
    }

    public function filter(array $possibleNumbers): array
    {
        $numbers = [];

        foreach ($possibleNumbers as $number) {
            if (strlen($number) === $this->answer) {
                $numbers[] = $number;
            }
        }

        return $numbers;
    }

    public function canBeAsked(): bool
    {
        return count($this->possibleDigits) > 1;
    }

    public function setAnswer($answer): void
    {
        $this->answer = (int) $answer;
    }
}
