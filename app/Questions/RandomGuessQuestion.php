<?php

namespace App\Questions;

class RandomGuessQuestion extends AbstractQuestion
{
    protected $answer;
    protected int $guess;

    public function initialise(array $possibleNumbers): void
    {
        $this->guess = $possibleNumbers[array_rand($possibleNumbers)];
    }

    public function getQuestion(): string
    {
        return sprintf('Is your number %s?', number_format($this->guess));
    }

    public function getPossibleAnswers(): array
    {
        return [
            true => 'Yes',
            false => 'No',
        ];
    }

    public function filter(array $possibleNumbers): array
    {
        $numbers = [];

        foreach ($possibleNumbers as $number) {
            if ($this->answer && $number === $this->guess) {
                $numbers[] = $number;
            }

            if (!$this->answer && $number !== $this->guess) {
                $numbers[] = $number;
            }
        }

        return $numbers;
    }

    public function setAnswer($answer): void
    {
        $this->answer = (bool) $answer;
    }

    public function jsonSerialize()
    {
        return parent::jsonSerialize() + [
                'guess' => $this->guess,
            ];
    }

    public function getContext(): array
    {
        return ['guess' => $this->guess];
    }

    public function setContext(array $context): void
    {
        $this->guess = (int) $context['guess'];
    }
}
