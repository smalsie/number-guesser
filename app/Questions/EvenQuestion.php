<?php

namespace App\Questions;

class EvenQuestion extends AbstractQuestion
{
    public function getQuestion(): string
    {
        return 'Is your number even?';
    }

    public function filter(array $possibleNumbers): array
    {
        $numbers = [];

        foreach ($possibleNumbers as $number) {
            $isEven = ($number % 2) === 0;

            if ($isEven === $this->answer) {
                $numbers[] = $number;
            }
        }

        return $numbers;
    }

    public function setAnswer($answer): void
    {
        $this->answer = (bool) $answer;
    }
}
