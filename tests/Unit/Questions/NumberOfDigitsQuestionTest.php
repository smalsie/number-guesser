<?php

namespace Tests\Unit\Questions;

use App\Questions\EvenQuestion;
use App\Questions\NumberOfDigitsQuestion;
use Tests\TestCase;

class NumberOfDigitsQuestionTest extends TestCase
{
    /** @test @dataProvider filtersProvider */
    public function it_filters_correctly(array $input, int $answer, array $expected): void
    {
        $question = new NumberOfDigitsQuestion();
        $question->setAnswer($answer);

        $this->assertEquals($expected, $question->filter($input));
    }

    public function filtersProvider(): array
    {
        return [
            [
                'input' => [0, 1, 2, 3, 4, 5, 50, 100],
                'answer' => 1,
                'expected' => [0, 1, 2, 3, 4, 5],
            ],
            [
                'input' => [0, 1, 2, 3, 4, 5, 50, 100],
                'answer' => 2,
                'expected' => [50],
            ],
            [
                'input' => [0, 1, 2, 3, 4, 5, 50, 100],
                'answer' => 3,
                'expected' => [100],
            ],
            [
                'input' => [0, 1, 20, 200, 5000, 12000, 750000],
                'answer' => 6,
                'expected' => [750000],
            ],
        ];
    }

    /** @test @dataProvider answersProvider */
    public function it_returns_the_correct_possible_answers(array $input, array $expected): void
    {
        $question = new NumberOfDigitsQuestion();
        $question->initialise($input);

        $this->assertEquals($expected, $question->getPossibleAnswers());
    }

    public function answersProvider(): array
    {
        return [
            [
                'input' => [0, 1, 2, 3, 4, 5, 50, 100],
                'expected' => [
                    1 => 1,
                    2 => 2,
                    3 => 3,
                ],
            ],
            [
                'input' => [0, 1, 2],
                'expected' => [
                    1 => 1,
                ],
            ],
            [
                'input' => [20, 200, 12000, 750000],
                'expected' => [
                    2 => 2,
                    3 => 3,
                    5 => 5,
                    6 => 6,
                ],
            ],
        ];
    }

    /** @test @dataProvider canBeAskedProvider */
    public function it_correctly_returns_can_be_asked(array $input, bool $expected): void
    {
        $question = new NumberOfDigitsQuestion();
        $question->initialise($input);

        $this->assertEquals($expected, $question->canBeAsked());
    }

    public function canBeAskedProvider(): array
    {
        return [
            [
                'input' => [0, 1, 2, 3, 4, 5, 50, 100],
                'expected' => true,
            ],
            [
                'input' => [0, 1, 2],
                'expected' => false,
            ],
            [
                'input' => [20, 200, 12000, 750000],
                'expected' => true,
            ],
        ];
    }
}
