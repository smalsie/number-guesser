<?php

namespace Tests\Unit\Questions;

use App\Questions\GreaterThanQuestion;
use App\Questions\MultipleOfQuestion;
use Tests\TestCase;

class MultipleOfQuestionTest extends TestCase
{
    /** @test @dataProvider filtersProvider */
    public function it_filters_correctly(int $multiple, array $input, bool $answer, array $expected): void
    {
        $question = new MultipleOfQuestion();
        $question->setContext(['multiple' => $multiple]);
        $question->setAnswer($answer);

        $this->assertEquals($expected, $question->filter($input));
    }

    public function filtersProvider(): array
    {
        return [
            [
                'multiple' => 3,
                'input' => [0, 1, 2, 3, 4, 5],
                'answer' => true,
                'expected' => [3],
            ],
            [
                'multiple' => 3,
                'input' => [0, 1, 2, 3, 4, 5],
                'answer' => false,
                'expected' => [0, 1, 2, 4, 5],
            ],
            [
                'multiple' => 10,
                'input' => [100, 150, 233, 235],
                'answer' => true,
                'expected' => [100, 150],
            ],
        ];
    }
}
