<?php

namespace Tests\Unit\Questions;

use App\Questions\GreaterThanQuestion;
use Tests\TestCase;

class GreaterThanQuestionTest extends TestCase
{
    /** @test @dataProvider filtersProvider */
    public function it_filters_correctly(int $number, array $input, bool $answer, array $expected): void
    {
        $question = new GreaterThanQuestion();
        $question->setContext(['number' => $number]);
        $question->setAnswer($answer);

        $this->assertEquals($expected, $question->filter($input));
    }

    public function filtersProvider(): array
    {
        return [
            [
                'number' => 3,
                'input' => [0, 1, 2, 3, 4, 5],
                'answer' => true,
                'expected' => [4, 5],
            ],
            [
                'number' => 3,
                'input' => [0, 1, 2, 3, 4, 5],
                'answer' => false,
                'expected' => [0, 1, 2, 3],
            ],
            [
                'number' => 233,
                'input' => [100, 150, 233, 235],
                'answer' => true,
                'expected' => [235],
            ]
        ];
    }
}
