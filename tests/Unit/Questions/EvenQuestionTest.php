<?php

namespace Tests\Unit\Questions;

use App\Questions\EvenQuestion;
use Tests\TestCase;

class EvenQuestionTest extends TestCase
{
    /** @test @dataProvider filtersProvider */
    public function it_filters_correctly(array $input, bool $answer, array $expected): void
    {
        $question = new EvenQuestion();
        $question->setAnswer($answer);

        $this->assertEquals($expected, $question->filter($input));
    }

    public function filtersProvider(): array
    {
        return [
            [
                'input' => [0, 1, 2, 3, 4, 5],
                'answer' => true,
                'expected' => [0, 2, 4],
            ],
            [
                'input' => [0, 1, 2, 3, 4, 5],
                'answer' => false,
                'expected' => [1, 3, 5],
            ],
            [
                'input' => [100, 150, 233, 235],
                'answer' => true,
                'expected' => [100, 150],
            ]
        ];
    }
}
